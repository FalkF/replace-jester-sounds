# DCS World - Replace Jester Sounds

AutoIt script used to replace the soundfiles of Jester for the DCS World F14 Module.

Requirements:
- [AutoIt 3](https://www.autoitscript.com/site/autoit/downloads/)


Usage:
- Convert the new files to ogg format and place them into a new folder named `ogg` in the script directory
- Set the const `$JESTER_FOLDER` in line 5 to the right path for your PC
- Run the script

Example Mod: [Jester To R2D2](https://www.digitalcombatsimulator.com/en/files/3303886/)