#include <FileConstants.au3>
#include <File.au3>
#include <Array.au3>

Const $JESTER_FOLDER = "C:\Eagle Dynamics\DCS World\Mods\aircraft\F14\Sounds\Jester"

DirCopy($JESTER_FOLDER, @ScriptDir & "\Jester", $FC_OVERWRITE)

$jesterFiles = _FileListToArrayRec(@ScriptDir & "\Jester", "*",  $FLTAR_FILES + $FLTAR_NOSYSTEM + $FLTAR_NOHIDDEN + $FLTAR_NOLINK, $FLTAR_RECUR )
$customFiles = _FileListToArrayRec(@ScriptDir & "\ogg", "*",  $FLTAR_FILES + $FLTAR_NOSYSTEM + $FLTAR_NOHIDDEN + $FLTAR_NOLINK, $FLTAR_RECUR )

_ArrayDelete($jesterFiles, 0)
For $file in $jesterFiles
   FileCopy(@ScriptDir&"\ogg\"&$customFiles[Random(1, $customFiles[0], 1)], @ScriptDir&"\Jester\"&$file, $FC_OVERWRITE)
  ;FileCopy(@ScriptDir&"\ogg\"&$customFiles[Random(1, $customFiles[0], 1)], $JESTER_FOLDER&"\"&$file, $FC_OVERWRITE) ; uncomment this line to directly install the sounds
Next
